#include <iostream>
#include<Singleton.h>

using namespace std;

int main()
{

    Singleton* s = Singleton::getInstance();
    Singleton* r = Singleton::getInstance();
    cout << s << endl;
    cout << r << endl;

    delete r;

    cout << s << endl;
    cout << r << endl;

    delete s;
    delete r;

    cout << s << endl;
    cout << r << endl;

    return 0;
}
