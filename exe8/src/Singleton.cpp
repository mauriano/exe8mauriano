#include "Singleton.h"

Singleton* Singleton::instance = 0;

Singleton* Singleton::getInstance(){
    if(!instance){
        instance = new Singleton();
    }
    return instance;
}


Singleton::Singleton()
{
    //ctor
}

Singleton::~Singleton()
{
    //dtor
}
